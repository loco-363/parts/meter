# ***********************************************
# ***         Loco363 - Parts - Meter         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Meter(generic.bases.Drill):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			70,
			107.5,
			center=True,
			properties={
				'fill': self.COLOR_UNI
			}
		))
		
		# indicator area
		self.add(gen_draw.shapes.Rectangle(
			c,
			60,
			64,
			center=True,
			properties={
				'fill': 'white'
			}
		))
		
		# coords
		self.add(generic.CoordsAuto(c, pos=(0, -5, 'middle')))
		
		# mounting holes
		a = 70/2 - 7
		b = 107.5/2-10
		self.add(generic.DrillHole(C(c, ( a,  b)), [4, 6], 'N => 6RZ', pos=( 6, 4,   'end'), cpos=( 6, -7, 'end')))
		self.add(generic.DrillHole(C(c, (-a,  b)), [4, 6], 'N => 6RZ', pos=(-6, 4, 'start'), cpos=(-6, -7, 'start')))
		self.add(generic.DrillHole(C(c, ( a, -b)), [4, 6], 'N => 6RZ', pos=( 6, 4,   'end'), cpos=( 6, -7, 'end')))
		self.add(generic.DrillHole(C(c, (-a, -b)), [4, 6], 'N => 6RZ', pos=(-6, 4, 'start'), cpos=(-6, -7, 'start')))
		
		# panel cut-out
		#TODO
	# constructor
# class Meter


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Meter(), True))
